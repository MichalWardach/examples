﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Gra
    {
        Plansza p;
        bool[,] kontener;

        public Gra()
        {
            p = new Plansza();
            kontener = new bool[10, 10];
        }


        public void symulacja()
        {
            p.ustawPlansze();
            kontener = p.kopiujPlansze();
            int iter =0;

            while (p.czyZycie(kontener))
            {
                p.wyswietl();
                Console.ReadLine();
                p.iteracja();
                kontener = p.kopiujPlansze();
                Console.Clear();
                iter++;
            }
            Console.Write("Ilosc iteracji gry: " + iter);
            System.Threading.Thread.Sleep(3000);
        }

    }
}
