﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Plansza
    {
        private char zywa = '#';
        private char martwa = 'O';
        private static int wiersze = 10;
        private static int kolumny = 10;
        private bool[,] plansza;

        public Plansza ()
        {
            plansza = new bool[wiersze, kolumny];
        }
        public void ustawPlansze()
        {
            Random random = new Random();

            for (int i = 0; i < 80; i++)
            {
                int x = random.Next(10);
                int y = random.Next(10);
                plansza[x, y] = true;
            }

        }



        public void wyswietl()
        {
            for (int i = 0; i < kolumny; i++)
            {

                for (int j = 0; j < wiersze; j++)
                {
                    if (plansza[i, j])
                        Console.Write(" " + zywa + " ");
                    else
                        Console.Write(" " + martwa + " ");

                }
                Console.Write("\n");
            }
        }


        public void reprodukcja (int x, int y)
        {
            for (int i = x - 1; i <= x + 1; i++)
            {
                if ((i < 0 || i > wiersze - 1) == false)
                {
                    for (int j = y - 1; j <= y + 1; j++)
                    {
                        if (((j < 0 || j > kolumny - 1) == false) && ((i == x && j == y)) == false)
                        {
                            if (plansza[i, j])
                                plansza[i, j] = false;
                        }
                    }
                }
            }
            plansza[x, y] = true;
        }

        public int sasiedzi(int x, int y)
        {
            int liczbaSasiadow = 0;

            for (int i = x - 1; i <= x + 1; i++)
            {
                if ((i < 0 || i > wiersze - 1) == false)
                {
                    for (int j = y - 1; j <= y + 1; j++)
                    {
                        if (((j < 0 || j > kolumny - 1) == false) && ((i == x && j == y)) == false)
                        {
                            if (plansza[i, j])
                                liczbaSasiadow++;
                        }
                    }
                }
            }
            return liczbaSasiadow;

        }


        public bool ewolucja(int x, int y)
        {
            int zywych = sasiedzi(x, y);
            if (plansza[x, y])
            {
                if ((zywych < 2) || (zywych > 3))
                {
                    return false;
                }

                else if ((zywych == 2) || (zywych == 3))
                {
                    return true;
                }
                else
                    return false;
            }
            else if ((plansza[x, y] == false) && (zywych == 3))
            {
                reprodukcja(x, y);
                return true;
            }
            else
            {
                return false;

            }

        }

        public void iteracja()
        {
            for (int i = 0; i <= wiersze - 1; ++i)
            {
                for (int j = 0; j <= kolumny - 1; ++j)
                {
                    plansza[i, j] = ewolucja(i, j);
                }
            }

        }

        public bool czyZycie(bool[,] tmp)
        {
            for (int i = 0; i <= wiersze - 1; ++i)
            {
                for (int j = 0; j <= kolumny - 1; ++j)
                {
                    if (tmp[i, j] == true)
                        return true;
                }
            }
            return false;

        }

        public bool [,] kopiujPlansze ()
        {
            bool[,] tmp = new bool[wiersze, kolumny];
            tmp = plansza;
            return tmp;
        }

    }
}
