import java.util.ArrayList;
import java.util.Random;
import java.util.List;

/**
 * Created by Michał on 2017-03-08.
 */
public class Player {
    private int pkt;
    private List <Card> hand = new ArrayList<Card>();

    public Player (){
        pkt = 0;
    };

    public void randomCard (Deck D){
        int lp = 0;
        Card drawn = new Card ("drawn", '!', 100); // wylosowana karta
        Random generator = new Random();
        lp = generator.nextInt(51);
        if (D.getCard(lp).getValue() == 100)
            randomCard(D);
        else {
            if (D.getCard(lp).getSymbol() == "ACE") {
                switch (oRe(checkScore())) {
                    case 11:
                        pkt += D.getCard(lp).getValue();
                        break;
                    case 1:
                        pkt += D.getCard(lp).getValue() - 10;
                        break;
                }
                hand.add(D.getCard(lp));
                D.changeCard(lp, drawn);
            }
            else {
                pkt += D.getCard(lp).getValue();
                hand.add(D.getCard(lp));
                D.changeCard(lp, drawn);
            }
        }
    }

    public int checkScore(){
        return pkt;
    }

    public void showHand (String S){
        System.out.println("_____________________");
            System.out.println("KARTY " + S);
        for (int i = 0; i < hand.size(); i++ ){
            hand.get(i).showCard();
        }
        if (S == "GRACZA")
        System.out.println("suma: " + pkt);
        System.out.println("_____________________");

    }

    private int oRe (int sum){
        int tmp = 0;
        tmp = (sum + 11) < 21 ? 11 : 1;
        return tmp;
    }

}