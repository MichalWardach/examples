/**
 * Created by Michał on 2017-03-08.
 */
public class Card {

    private String symbol;
    private char color;
    private int value;

    private Card() {
        symbol = " ";
        color = ' ';
        value = 0;
    };

    public Card(String name, char c, int v) {
        symbol = name;
        color = c;
        value = v;
    };

    public int getValue(){
        return value;
    }

    public String getSymbol(){
        return symbol;
    }

    public void showCard (){
        System.out.println(symbol + "   " + color + "   " + value + "\n");
    }

}
