import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.lang.Math.abs;

/**
 * Created by Michał on 2017-04-03.
 */

public class Sierpinski {

    public int x1, y1, x2, y2, x3, y3;
    public int tmpx, tmpy;
    public List <Point> points = new ArrayList <Point>();

    public Sierpinski (){

        x1 = 512;
        y1 = 109;
        x2 = 146;
        y2 = 654;
        x3 = 876;
        y3 = 654;
        tmpx = 512;
        tmpy = 382;
        Point pkt = new Point();
        pkt.x = tmpx;
        pkt.y = tmpy;
        points.add(pkt);
    }
    public void countTriangls (){

        int los = 0;
        Random rand = new Random();
        los = rand.nextInt(3);
        Point pkt = new Point();

        switch (los){
            case 0:
                tmpx -= (tmpx - x1)/2;
                tmpy -= (tmpy - y1)/2;
                System.out.println("x: " + tmpx + "\ty: " + tmpy);
                break;
            case 1:
                tmpx -= (tmpx - x2)/2;
                tmpy -= (tmpy - y2)/2;
                break;
            case 2:
                tmpx -= (tmpx - x3)/2;
                tmpy -= (tmpy - y3)/2;
                break;
            default:
                countTriangls();
        }
        Point P = new Point();
        P.x = tmpx;
        P.y = tmpy;
        points.add(P);
    }

}