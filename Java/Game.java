import java.util.Scanner;

/**
 * Created by Michał on 2017-03-08.
 */
public class Game {

    private Player Gracz = new Player();
    private Player Krupier = new Player();

    public Game (){};

    public int isWinner () {
        if (Krupier.checkScore() > 21) {
            System.out.println("\nZWYCIESTWO GRACZA !");
        }
        else if (Gracz.checkScore() > 21) {
            System.out.println("\nZWYCIESTWO KRUPIERA !");
        }
        else if (Gracz.checkScore() == Krupier.checkScore()){
            System.out.println("\nZWYCIESTWO KRUPIERA !");
        }
        else if (Gracz.checkScore() == 21) {
            System.out.println("\nZWYCIESTWO GRACZA !");
        }
        else if (Krupier.checkScore() == 21) {
            System.out.println("\nZWYCIESTWO KRUPIERA !");
        }
        else if((Gracz.checkScore() > Krupier.checkScore()) & Gracz.checkScore() < 21){
            System.out.println("\nZWYCIESTWO GRACZA !");
        }
        else if((Krupier.checkScore() > Gracz.checkScore()) & Krupier.checkScore() < 21) {
            System.out.println("\nZWYCIESTWO KRUPIERA !");
        }
        System.out.println("\n++++ WYYNIKI ++++\nGracz: " + Gracz.checkScore() +" pkt" + "\nKRUPIER: " + Krupier.checkScore() + " pkt");
            return 0;
    }

    public int isOverScore (){
        if ((Gracz.checkScore() >= 21) || (Krupier.checkScore() >= 21))
            return 1;
        else
            return 0;
    }

    public int match (Deck D){

        Scanner in = new Scanner(System.in);
        int a = 0, fl = 0;

        Gracz.randomCard(D);
        Gracz.randomCard(D);
        Gracz.showHand("GRACZA");

        Krupier.randomCard(D);
        Krupier.showHand("KRUPIERA");
        Krupier.randomCard(D);

        while ( isOverScore() != 1){
            System.out.println("Kolejna karta?\ntak -> 1\nnie -> 2 \nTwoj wybor: ");
            a = in.nextInt();
            if (a == 1) {
                Gracz.randomCard(D);
                Gracz.showHand("GRACZA");
                if (isOverScore() > 0)
                    break;
            }
            else if ( a == 2)
                System.out.println(" -> GRACZ PASS");
            else
                System.out.println("NIEPOPRAWNA OPCJA");

            if (Krupier.checkScore() > 16) {
                System.out.println(" -> KRUPIER PASS");
                fl = 1;
            }
            else {
                System.out.println(" -> KRUPIER LOSUJE");
                Krupier.randomCard(D);
                if (isOverScore() > 0)
                    break;
            }
                //Krupier.showHand("KRUPIERA");

            if ( a == 2 & fl == 1)
                break;
        }

        isWinner();
        return 0;

        }

}
