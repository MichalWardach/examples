/**
 * Created by Michał on 2017-03-08.
 */
public class Deck {

    private Card deck [] = new Card [52];

    public Deck () {};

    private int createColor (char mark, int position){
        Card Ace = new Card("ACE", mark, 11);
        deck [position] = Ace;
        position ++;
        Card King = new Card("KING", mark, 10);
        deck [position] = King;
        position ++;
        Card Dame = new Card("DAME", mark, 10);
        deck [position] = Dame;
        position ++;
        Card Jack = new Card("JACK", mark, 10);
        deck [position] = Jack;
        position ++;
        Card Ten = new Card("TEN", mark, 10);
        deck [position] = Ten;
        position ++;
        Card Nine = new Card("NINE", mark, 9);
        deck [position] = Nine;
        position ++;
        Card Eight = new Card("EIGHT", mark, 8);
        deck [position] = Eight;
        position ++;
        Card Seven = new Card("SEVEN", mark, 7);
        deck [position] = Seven;
        position ++;
        Card Six = new Card("SIX", mark, 6);
        deck [position] = Six;
        position ++;
        Card Five = new Card("FIVE", mark, 5);
        deck [position] = Five;
        position ++;
        Card Four = new Card("FOUR", mark, 4);
        deck [position] = Four;
        position ++;
        Card Three = new Card("THREE", mark, 3);
        deck [position] = Three;
        position ++;
        Card Two = new Card("TWO", mark, 2);
        deck [position] = Two;
        position ++;

        return position;
    }

    public void setDeck (){
        int tmp = 0;
        tmp = createColor('S', 0);
        tmp = createColor('H', tmp);
        tmp = createColor('D', tmp);
        tmp = createColor('C', tmp);

    }

    public Card getCard (int idx){
        return deck[idx];
    }

    public void changeCard (int idx, Card C){
        deck[idx] = C;
    }

    public void showDeck (){
        for (int i = 0; i < deck.length ; i++){
            deck[i].showCard();
        }
    }

}
