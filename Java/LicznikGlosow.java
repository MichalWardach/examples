/**
 * Created by Michał on 2017-04-02.
 */
// ***********************************************************************
//  LicznikGlosow.java
//
//  Wykorzystuje GUI oraz listenery eventow do glosowania
//  na dwoch kandydatow -- Jacka i Placka.
//
// ***********************************************************************
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.Socket;
import java.util.Scanner;

import javax.swing.*;

public class LicznikGlosow extends JApplet
{
    private int APPLET_WIDTH = 300, APPLET_HEIGHT=100;
    private int glosyDlaJacek, glosyDlaPlacek;
    private JLabel labelJacek, labelPlacek;
    private JButton Jacek, Placek;

    final int port = 50000;
    final  String host = "local host";
    Socket sock;



    // ------------------------------------------------------------
    //  Ustawia GUI
    // ------------------------------------------------------------
    public void init () {


        glosyDlaJacek = 0;
        glosyDlaPlacek = 0;

        try {
            sock = new Socket(host, port);
            PrintWriter outp = new PrintWriter(sock.getOutputStream(), true);

            Jacek = new JButton("Glosuj na Jacka!");
            Jacek.addActionListener(new JacekButtonListener(outp));
            Placek = new JButton("Glosuj na Placka!");
            Placek.addActionListener(new PlacekButtonListener(outp));

            labelJacek = new JLabel("Glosy dla Jacka: " + Integer.toString(glosyDlaJacek));
            labelPlacek = new JLabel("Glosy dla Placka: " + Integer.toString(glosyDlaPlacek));

            Container cp = getContentPane();
            cp.setBackground(Color.cyan);
            cp.setLayout(new FlowLayout());
            cp.add(Jacek);
            cp.add(labelJacek);
            cp.add(Placek);
            cp.add(labelPlacek);

            setSize(APPLET_WIDTH, APPLET_HEIGHT);


        } catch (IOException e) {
            System.err.println("Could not connect to " + host + ":" + port);
            e.printStackTrace();
            System.exit(-1);
        }


    }

    // *******************************************************************
    //  Reprezentuje listener dla akcji wcisniecia przycisku
    // *******************************************************************
    private class JacekButtonListener implements ActionListener
    {
        private PrintWriter pWriter;

        public JacekButtonListener(PrintWriter pWriter)
        {
            this.pWriter = pWriter;
        }

        public void actionPerformed (ActionEvent event)
        {
            glosyDlaJacek++;
            this.pWriter.println("Jacek");
            labelJacek.setText ("Glosy dla Jacka: " + Integer.toString (glosyDlaJacek));
            repaint();
        }
    }

    private class PlacekButtonListener implements ActionListener
    {
        private PrintWriter pWriter;

        public PlacekButtonListener(PrintWriter pWriter)
        {
            this.pWriter = pWriter;
        }

        public void actionPerformed (ActionEvent event)
        {
            glosyDlaPlacek++;
            this.pWriter.println("Placek");
            labelPlacek.setText ("Glosy dla Placka: " + Integer.toString (glosyDlaPlacek));
            repaint ();
        }
    }
}
