/**
 * Created by Michał on 2017-04-02.
 */
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static final int PORT = 50000;
    static boolean tmp = true;


    public static void main(String[] args) throws IOException {

        int jacki = 0;
        int placki = 0;

        ServerSocket serv = null;

        try
        {
            serv = new ServerSocket(PORT);
        }
        catch(IOException e)
        {
            System.err.println("Blad utworzenia serwera!");
            System.exit(1);
        }

        Socket sock;
        sock = serv.accept();
        System.out.println("Jest polaczenie: " + sock);

        BufferedReader inp = new BufferedReader(new InputStreamReader(sock.getInputStream()));

        String info;
        try
        {
            while(tmp)
            {
                info = inp.readLine();

                if (info.equals("Jacek"))
                    jacki ++;
                if (info.equals("Placek"))
                    placki ++;

                System.out.println("Glosow na Jacka: " + jacki + "\t Glosow na Placka: " + placki +
                        "\t Glosow w sumie: " + (placki + jacki));
            }
        }
        catch(Exception e)
        {
            tmp = false;
        }


        inp.close();
        sock.close();
        serv.close();
    }
}
