#ifndef WIERZCHOLEK_HH
#define WIERZCHOLEK_HH
#include <iostream>

using namespace std;

/**
  * @brief klasa modelujaca pojecie Wierzcholka
  */
class Wierzcholek
{
    public:
/**
  * @brief wartosc, indeks wierzcholka
  */
    int wartosc;
/**
  * @brief konstruktor klasy Wierzcholek
  */
    Wierzcholek () {wartosc = 0;}
/**
  * @brief destruktor klasy Wierzcholek
  */
    ~Wierzcholek (){};
/**
  * @brief tworzy wierzcholek o podanym indeksie
  *
  * @param indeks wierzcholka
  */
    void utworz (int x){wartosc = x;}
/**
  * @brief wyswietla indeks wierzcholka
  */
    void pokaz () { cout << wartosc; }
/**
  * @brief pobiera indeks wierzcholka
  */
    int pobierz () {return wartosc; }

};


#endif
