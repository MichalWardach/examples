#ifndef TABLICA_HH
#define TABLICA_HH
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <fstream>
#include <windows.h>

using namespace std;


/**
* @brief Klasa modelujaca szablon tabliy
*/
template < int ilosc, class typ >
class tablica
{
public:
/**
* @brief tablica zadanego typu
*/
    typ tab[ ilosc ];

/**
* @brief konstruktor klasy tablica
*/

    tablica (){};

/**
  * @brief pokazuje zawartosc tablicy
  */
    void pokaz();

/**
  * @brief uzupelnia tablice pseudolosowymi liczbami
  */
    void uzupelnij();

/**
  * @brief szybkie sortowanie
  *
  * @param lewy indeks poczatka tablicy
  *
  * @param prawy indeks konca tablicy
  */
    void quickSort(typ left, typ right);

/**
  * @brief scala dwie tablice w jedna,
  *        pomocnicza do sortowania przez scalanie
  *
  * @param lewy indeks poczatka tablicy
  *
  * @param prawy indeks konca tablicy
  *
  * @param prawy indeks srodka tablicy
  */
    void scal( typ lewy, typ srodek, typ prawy);

/**
  * @brief sortowanie przez scalanie
  *
  * @param lewy indeks poczatka tablicy
  *
  * @param prawy indeks konca tablicy
  */
    void sortowaniePrzezScalanie(typ lewy, typ  prawy);

/**
  * @brief sortowanie przez scalanie
  */
    void shellSort();


/**
  * @brief odczytywanie danych z pliku
  *
  * @param adres tablicy z plikami
  */
    void odczytajPlik(char &argv);

/**
  * @brief odczytywanie danych z pliku
  */
    void zapiszDane();

};



template < int ilosc, class typ >
void tablica <ilosc, typ > :: uzupelnij ()
{
    srand ((unsigned int) time(NULL));
    for ( int i = 0; i< ilosc; ++i)
    {
        tab[i]= rand()%100;
    }

}



template < int ilosc, class typ >
void tablica <ilosc, typ > :: pokaz ()
    {
        for ( int i = 0; i< ilosc; ++i)
        {
           cout << tab[ i ] << " ";
        }

   }


template < int ilosc, class typ >
void tablica <ilosc, typ > :: quickSort (typ lewy, typ prawy)
{


    typ i = lewy;
    typ j = prawy;
    typ x = tab[( lewy + prawy ) / 2 ];   //znalezienie srodka tablicy
    do
    {
        while( tab[ i ] < x )
             i++;

        while( tab[ j ] > x )
             j--;

        if( i <= j )
        {
            swap( tab[ i ], tab[ j ] );    //odpowiednia zamiana elementow tablicy

            i++;
            j--;
        }
    } while( i <= j );

    if( lewy < j )
    {
        quickSort(lewy, j );
    }
    if( prawy > i )
    {
        quickSort(i, prawy);
    }


}


template < int ilosc, class typ >
void tablica <ilosc, typ > :: scal( typ lewy, typ srodek, typ prawy)
{
    typ i, j;
    typ pom[ilosc];  // tablica pomocnicza

  for(i = srodek + 1; i>lewy; i--) //zapisujemy lewa czsc podtablicy w tablicy pomocniczej
  {
      pom[i-1] = tab[i-1];
  }


  for(j = srodek; j<prawy; j++)  //zapisujemy prawa czesc podtablicy w tablicy pomocniczej
  {
      pom[prawy + srodek-j] = tab[j+1];
  }



  for(typ k = lewy; k <= prawy; k++)    //scalenie dw�ch podtablic pomocniczych i zapisanie ich w koncowej tablicy
  {
      if(pom[j] < pom[i])
      {
         tab[k] = pom[j--];
      }
      else
      {
          tab[k] = pom[i++];
      }

  }

}


template < int ilosc, class typ >
void tablica <ilosc, typ > :: sortowaniePrzezScalanie(typ lewy, typ prawy)
{

  if(prawy <= lewy) return;  //gdy mamy jeden element nie ma co sortowac
  typ srodek = (prawy+lewy)/2;    //znajdujemy srodek podtablicy

  sortowaniePrzezScalanie( lewy, srodek);  //dzielimy tablice na dwie czesci: lewa i prawa
  sortowaniePrzezScalanie( srodek+1, prawy);

  scal( lewy, srodek, prawy);  //scalamy dwie posrotwane tablice
}


template < int ilosc, class typ >
void tablica <ilosc, typ > :: shellSort()
{
   typ h,i,j, t;
   for(h = 1; h <= ilosc/9; h = h*3+1);   // petla wyznaczajaca odleglosc pomiedzy elementami
   while(h > 0)
   {
      for(i=h; i<ilosc; i++)
      {
         t = tab[i];
         j = i-h;
         while ((j >= 0) && (tab[j] > t))   //porownywanie i ewentualna zamiana elementow
         {
            tab[j+h] = tab[j];
            j-=h;
         }
         tab[j+h] = t;
      }
      h /= 3;
   }
}

template < int ilosc, class typ >
void tablica <ilosc, typ > :: odczytajPlik(char &argv)
{
  ifstream PlikWejscia;  //zmienia strumieniowa do pracy z plikiem
  int i;


    PlikWejscia.open("dozapisu.txt", std::ios::in);       // informacje o blednym dzialaniu z plikiem
    if(PlikWejscia == NULL)
      {
        cerr<<"\nNie otworzono pliku !";
      }

  if(!PlikWejscia.good())
     {
        cerr<<"\nBlad wczytywania tresci pliku.";
     }

	while( !PlikWejscia.eof())               // odczyt danych do znaku konca pliku
	{

		for (i = 0; i< ilosc; ++i)
		PlikWejscia >> tab[i];
	}

 	PlikWejscia.close();        //zamkniecie pliku

}


template < int ilosc, class typ >
void tablica <ilosc, typ > :: zapiszDane()
  {
  ofstream PlikWyjscia;               //zmienia strumieniowa do pracy z plikiem
  int i;

  PlikWyjscia.open( "dozapisu.txt", std::ios::out );
  if( PlikWyjscia.good() == true )           //sprawdzenie czy otwarcie zostalo poprawnie wykonane
	{

		for (i = 0; i< ilosc; ++i)
		PlikWyjscia << tab[i] << " ";


  } else cout << "Dostep do pliku zostal zabroniony!" << endl;



  PlikWyjscia.close();


}





#endif

