#ifndef SCENA_HH
#define SCENA_HH
#include "Wektor.hh"
#include "Macierz.hh"
#include "Manipulator.hh"
#include <vector>
#include <iostream>



/*************************************************************************/
//------------------------ Scena -----------------------------------------
/*************************************************************************/

/**
* @brief Klasa modeluje pojecie sceny jako miejsce zapisu wsp weirzcholkow
* 	dla pracy Gnuplota 
*/
class Scena{
	
	public:


/**
* @brief kontener typu vector na wspolrzedne przegubow jako wektory
*/
	std::vector<Wektor <double> > Wsp;


/**
 * @brief Zapsiuje wspolrzedne do pliku "poczatek.txt"
 *   
 */

	void ZapiszPozycje();

/**
 * @brief kopiuje obliczone pozycje przegubow i zapsuje do pliku 
 *   	dla pracy Gnuplota
 */

	void WezPozycje(Manipulator Man);
	
/**
* @brief konsruktor klasy Manipulator
*/
	Scena () {Wsp.clear();  
			Wektor<double> Wek;
			Wek.wstaw(0, 0);
			Wek.wstaw(1, 0);
			Wsp.push_back(Wek);}


};










#endif
