#include <iostream>
#include "grafNaList.hh"
#include "grafNaMac.hh"
#include "mst.hh"
#include <queue>
#include <set>
#include <vector>
#include <windows.h>
#include <deque>

using namespace std;



Mst algorytmKruskala (Graf G)
{
    Mst drzewo;
    Krawedz odczytana;
    int koszt = 0;

    for( size_t i = 0; i < G.listaKrawedzi.size(); i++ )            // kopiowanie krawedzi do kolejki priorytetowej
    {
        drzewo.dodajKrawedz(G.listaKrawedzi[i]);
    }

    for (int i = 0; i < G.ileWierzcholkow; i++)                 // petla dla wszystkich wierzcholkow
    {
        odczytana = drzewo.zdejmijKrawedz();

        if ((drzewo.czyOdwiedzony(odczytana.poczatek) == false) || (drzewo.czyOdwiedzony(odczytana.koniec) == false))   //czy ktorys z wierzcholkow nie nalezy do drzewa
        {
            koszt += odczytana.waga;
            if (drzewo.czyOdwiedzony(odczytana.poczatek) == false)    //czy tym wierzcholkwiem jest poczatek krawedzi
            {
                drzewo.dodajWierzcholek(odczytana.poczatek);
            }
            else if (drzewo.czyOdwiedzony(odczytana.koniec) == false)  //else: koniec krawedzi
            {
                drzewo.dodajWierzcholek(odczytana.koniec);
            }

        }
    }
        //cout <<"wszystkie? " << drzewo.czyWszystkie()<< endl;   //sprawdzenie czy wszystkie wierzcholki zostaly dodane
        cout << "koszt wg. Kruskala: "<< koszt << endl;

    return drzewo;                                            //// zwroc minimalne drzewo rozpinajace
}


Mst algorytmPrimaJarnika (Graf G)
{
    Mst drzewo;
    Krawedz tmp;
    int koszt = 0;
    drzewo.dodajWierzcholek(G.listaWierzcholkow[0]);         //wybor wierzcholka poczatkowego, tutaj peirwszy wierzcholek
    drzewo.znajdzIncydentne(G.listaWierzcholkow[0] , G);     // znalezienie krawedzi incydentnych i dodanie ich do kolejki
    for (int i = 0; i < G.ileWierzcholkow; i++)              // dopoki wszyskie wierzcholki nie zostaly dodane
    {
        tmp = drzewo.zdejmijIncydentna();
        if (!drzewo.czyOdwiedzony(tmp.koniec))              //sprawdzenie czy wierzcholek konca krawedzi incydentnej nalezy do drzewa
            {
            drzewo.dodajWierzcholek(tmp.koniec);            // jezeli nie nalezy to dodaj
            koszt += tmp.waga;
            drzewo.znajdzIncydentne(tmp.koniec, G);        //znajdz incydentne dla dla nowo dodanego wierzcholka
            }
    } cout << "koszt wg. Prima i Jarnika: "<< koszt << endl;
    //cout <<"wszystkie? " << drzewo.czyWszystkie()<< endl;   //sprawdzenie czy zostaly dodane wszystkie wierzcholki

    return drzewo;                                       // zwroc minimalne drzewo rozpinajace
}




int main(int argc, char **argv)
{

 LARGE_INTEGER frequency;
    LARGE_INTEGER t1, t2;

    double elapsedTime;
    QueryPerformanceFrequency(&frequency);


    GrafNaList graf;
    graf.odczytajGraf (**argv);
    //graf.wypiszGraf();
    // graf.dodajSasiadow();
    //graf.wypiszSasiadow ();
    cout << "***** Graf na liscie sasiedztwa *****" << endl;
    QueryPerformanceCounter(&t1);
    algorytmKruskala (graf);
    QueryPerformanceCounter(&t2);
    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    cout << "czas trwania algorytmu Kruskala:" << elapsedTime << " ms.\n" << endl;

    QueryPerformanceCounter(&t1);
    algorytmPrimaJarnika(graf);
    QueryPerformanceCounter(&t2);
    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    cout << "czas trwania algorytmu PrimaJarnika:" << elapsedTime << " ms.\n" << endl;



    GrafNaMac grafm;
    grafm.odczytajGraf (**argv);
    //grafm.wypiszGraf();
    //grafm.uzupelnijMacierz();
    //grafm.wypiszMacierz();
    cout << "***** Graf na macierzy sasiedztwa *****" << endl;
    QueryPerformanceCounter(&t1);
    algorytmKruskala (grafm);
    QueryPerformanceCounter(&t2);
    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    cout << "czas trwania algorytmu Kruskala:" << elapsedTime << " ms.\n" << endl;

    QueryPerformanceCounter(&t1);
    algorytmPrimaJarnika(grafm);
    QueryPerformanceCounter(&t2);
    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    cout << "czas trwania algorytmu PrimaJarnika:" << elapsedTime << " ms.\n" << endl;


    return 0;
}
