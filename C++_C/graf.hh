#ifndef GRAF_HH
#define GRAF_HH
#include <iostream>
#include <fstream>
#include <deque>
#include <vector>
#include "krawedz.hh"

using namespace std;

/**
  * @brief Klasa modelujaca graf
  */
class Graf
{
    public:

/**
  * @brief pole przechowujace ilosc wierzcholkow
  */
    int ileWierzcholkow;
/**
  * @brief pole przechowujace ilosc krawedzi
  */
    int ileKrawedzi;
/**
  * @brief lista krawedzi jako kolejka dwukierunkowa
  */
    deque < Krawedz > listaKrawedzi;
/**
  * @brief lista wierzcholkow jako kolejka dwukierunkowa
  */
    deque < Wierzcholek > listaWierzcholkow;

/**
  * @brief konstruktor klasy Graf
  */
    Graf ()
    {
        ileWierzcholkow = 0;
        ileKrawedzi = 0;
    }
/**
  * @brief destruktor kalsy Graf
  */
    ~Graf(){};

/**
  * @brief wypisuje reprezentacje grafu
  */
    void wypiszGraf();

/**
  * @brief odczytanie elementow grafu z pliku
  *
  * @param adres tablicy plikow wejsciowych
  */
    void odczytajGraf (char &argv);

};



#endif
