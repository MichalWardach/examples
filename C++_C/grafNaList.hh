#ifndef GRAFNATAB_HH
#define GRAFNATAB_HH
#include "graf.hh"
#include <vector>
#include <fstream>
#include <iomanip>


/**
  * @brief kalsa obrazujaca graf na podstawie listy sasiedztwa
  */
class GrafNaList: public Graf
{
    public:
/**
  * @brief tablica tablic dla sasiadow poszczegolnych wierzcholkow
  */
    vector<int> Sasiedzi[100];
/**
  * @brief konstruktor klasy
  */
    GrafNaList (){};
/**
  * @brief destruktor klasy
  */
    ~GrafNaList (){};
/**
  * @brief dodaje sasiadow poszczegolnych wierzcholkow
  */
    void dodajSasiadow ();
/**
  * @brief wypisuje wszystkich sasiadow kolejnych wierzcholkow
  */
    void wypiszSasiadow();
};




#endif
