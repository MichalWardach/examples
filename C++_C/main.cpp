#include "Wektor.hh"
#include "Macierz.hh"
#include "Manipulator.hh"
#include "Scena.hh"
#include "lacze_do_gnuplota.hh"
#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <fstream>
#include <iomanip>

int main(int argc, char **argv)
{
std::ifstream PlikWejscia;
std::ofstream PlikWyjscia;
Manipulator Man;
Wektor<double> Wek;
Wektor <double> Wek2;
double tmp;
Scena Scena;


  PzG::LaczeDoGNUPlota  Lacze;
  Lacze.DodajNazwePliku("poczatek.txt",PzG::RR_Ciagly,6);
  Lacze.DodajNazwePliku("poczatek.txt",PzG::RR_Punktowy,2);
  Lacze.Inicjalizuj();
  Lacze.ZmienTrybRys(PzG::TR_2D);

  Lacze.UstawZakresY(-100,300);
  Lacze.UstawZakresX(-200,200);



/////////////////
  PlikWejscia.open(argv[1], std::ios::in);
    if(PlikWejscia== NULL)
      {
	std::cerr<<"\nNie otworzono pliku !";
	return 0;
      }

  if(!PlikWejscia.good())
    {
    std::cerr<<"\nBlad wczytywania konfiguracji manipulatora ";
    return -1;
    }


  while( !PlikWejscia.eof() )
        {
            PlikWejscia >> tmp;
		Man.DlugosciOgniw.push_back(tmp);
        }
  PlikWejscia.close();

	Man.ZrobPion();
	Man.ObliczPoz();
	Man.ZapiszPoczatek();
	std::cout << Man << std::endl;
	Lacze.Rysuj();
	sleep(2);
	Man.KatNachylenia.clear();
	Man.Pozycje.clear();

    PlikWejscia.open(argv[2], std::ios::in);
    if(PlikWejscia== NULL)
      {
	std::cerr<<"\nNie otworzono pliku !";
	return 0;
      }

  if(!PlikWejscia.good())
    {
    std::cerr<<"\nBlad wczytywania konfiguracji manipulatora ";
    return -1;
    }



  while( !PlikWejscia.eof() )
        {
            PlikWejscia >> tmp;
		Man.KatNachylenia.push_back(tmp);
        }
  PlikWejscia.close();

	Wek.wstaw(0, 0);
	Wek.wstaw(1, 0);
	Man.Pozycje.push_back(Wek);
	Man.ObliczPoz();
	std::cout << Man << std::endl;
	Scena.Wsp.clear();
	Scena.WezPozycje(Man);
	Scena.ZapiszPozycje();
	Lacze.Rysuj();
	Man.KatNachylenia.clear();
	sleep(1);

    PlikWejscia.open(argv[3], std::ios::in);
    if(PlikWejscia== NULL)
      {
	std::cerr<<"\nNie otworzono pliku !";
	return 0;
      }

  if(!PlikWejscia.good())
    {
    std::cerr<<"\nBlad wczytywania konfiguracji manipulatora ";
    return -1;
    }



  while( !PlikWejscia.eof() )
        {
            PlikWejscia >> tmp;
		Man.KatNachylenia.push_back(tmp);
        }
  PlikWejscia.close();

	Wek.wstaw(0, 0);
	Wek.wstaw(1, 0);
	Man.Pozycje.clear();
	Man.Pozycje.push_back(Wek);
	Man.ObliczPoz();
	std::cout << Man << std::endl;
	Scena.Wsp.clear();
	Scena.WezPozycje(Man);
	Scena.ZapiszPozycje();
	Lacze.Rysuj();





  	std::cout << "\n\nAby zakonczyc nacisnij ENTER ..." << std::flush;
	char b;
	std::cin >> std::noskipws >> b;
return 0;
}

