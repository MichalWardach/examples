#include "tablica.hh"

using namespace std;

int main(int argc, char **argv)
{
    tablica <10000, int> tab;  //rozmiar tablicy zmieniany recznie
    LARGE_INTEGER frequency;
    LARGE_INTEGER t1, t2;

    double elapsedTime;
    QueryPerformanceFrequency(&frequency);


    tab.uzupelnij();  //uzupelnienie tablicy i zapisanie jej do pliku
    tab.zapiszDane();
    int wybor;



    do
    {

        cout << "quickSort ->                 1 " << endl;
        cout << "sortowanie przez scalanie -> 2 " << endl;
        cout << "sortowanie met. Shella ->    3 " << endl;
        cout << "koniec dzialania programu -> 4 " << endl;
        cout << "Wybierz czynnosc: ";
        cin >> wybor;
		switch (wybor)
		{
			case 1:
                    tab.odczytajPlik(**argv);
                    QueryPerformanceCounter(&t1);
                    tab.quickSort(0, 10000);
                    QueryPerformanceCounter(&t2);
                    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
                    cout << "czas trwania algorytmu:" << elapsedTime << " ms.\n" << endl;
                    tab.zapiszDane();
                    tab.uzupelnij();
                    break;

			case 2:
                    tab.odczytajPlik(**argv);
                    QueryPerformanceCounter(&t1);
                    tab.sortowaniePrzezScalanie(0, 10000);
                    QueryPerformanceCounter(&t2);
                    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
                    cout << "czas trwania algorytmu:" << elapsedTime << " ms.\n" << endl;
                    tab.zapiszDane();
                    tab.uzupelnij();
                    break;

			case 3:
                    tab.odczytajPlik(**argv);
                    QueryPerformanceCounter(&t1);
                    tab.shellSort();
                    QueryPerformanceCounter(&t2);
                    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
                    cout << "czas trwania algorytmu:" << elapsedTime << " ms.\n" << endl;
                    tab.zapiszDane();
                    tab.uzupelnij();
                    break;

            case 4:
                    cout << "Zakonczono dzialanie programu." << endl;
                    break;

			default:
                    cout << "Niepoprawna opcja!" << endl;
                    break;


        }

	}while (wybor != 4);


 return 0;
}
