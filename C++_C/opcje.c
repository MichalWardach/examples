#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include "opcje.h"
#include "funkcje.h"

/* kody bledow programu */

#define OK 0
#define ZLA_OPCJA -1
#define BRAKNAZWY_IN -2
#define BRAKNAZWY_OUT -3
#define BRAKPROGU -4
#define BRAKPROMIENIA -5
#define BRAKPLIKU -6


/***********************************************************************/

int opcje_czytaj(int argc, char **argv, t_opcje *wybor)
{
    int i,j;
    int in_prog;
    int in_prom;
    char *nazwa_pliku_we, *nazwa_pliku_wy;

    wybor->prog=0;
    wybor->prom=0;
    wybor->negatyw=0;
    wybor->progowanie=0;
    wybor->rozmycie_poziome=0;
    wybor->konturowanie=0;
    wybor->wyswietlenie=0;
    wybor->zapisz=0;
    wybor->plik_in=NULL;
    wybor->plik_out=NULL;

    for(i=1;i<argc;i++)
    {
	if (argv[i][0] != '-')
	return -1; /* to nie jest opcja - brak "-" */

	switch (argv[i][1])
	{
	case 'i':
		{
		if (++i<argc)
		  	{
		    	nazwa_pliku_we=argv[i];
		    	if (strcmp(nazwa_pliku_we,"-")==0)
			wybor->plik_in=stdin;
		   	else
			wybor->plik_in=fopen(nazwa_pliku_we,"r");
		  	}
		else
		return BRAKNAZWY_IN; /* w wypadku opcji i wymagana jest nazwa pliku */
		break;
	    	}

	case 'p':
		{
	 	if (++i<argc)
	    		{
	      		if (sscanf(argv[i],"%d",&in_prog)==1) /* po p wymagamy podania progu */
				{
		 	 	wybor->progowanie=i; /* w celu uwzglednienia kolejnosci argumentow */
		  		wybor->prog=in_prog;
				}
	      		else
			return BRAKPROGU;  /* niepoprawny prog */
	     		}
	  	else
	    	return BRAKPROGU;  /* brak progu */
	  	break;
	  	}

	case 'r':
		{
	 	if (++i<argc)
	    		{
	      		if (sscanf(argv[i],"%d",&in_prom)==1) /* po r wymagamy podania promienia */
				{
		 	 	wybor->rozmycie_poziome=i; /* w celu uwzglednienia kolejnosci argumentow */
		  		wybor->prom=in_prom;
				}
	      		else
			return BRAKPROMIENIA;  /* niepoprawny promien */
	     		}
	  	else
	    	return BRAKPROMIENIA;  /* brak promienia */
	  	break;
	  	}

	 case 'o':
		{
		if (++i<argc)
		{
	     	nazwa_pliku_wy=argv[i];
			if (strcmp(nazwa_pliku_wy,"-")==0)
	        	{
			wybor->plik_out=stdout;  /* gdy "-" ustawiamy wyjscie na stdout */
			wybor->zapisz=0;
	        	}
	    		else
	        	{
			wybor->plik_out=fopen(nazwa_pliku_wy,"w"); /* otwieramy wskazany plik */
			wybor->zapisz=1;
	        	}
	  	}
		else
	    	return BRAKNAZWY_OUT;
	  	break;
	  	}

	case 'n': /* wykonujemy negatyw */
		{
	  	wybor->negatyw=i;
	  	break;
		}


	case 'k': /* wykonujemy konturowanie */
		{
	  	wybor->konturowanie=i;
	  	break;
		}

	case 'd': /* wyswietlamy */
		{
	  	wybor->wyswietlenie=1;
	  	break;
		}

	default: return ZLA_OPCJA;
	}
    }
    if (wybor->plik_in!=NULL)
    return OK;
    else
    return BRAKPLIKU;
}

/***************************************************************************/

int jaki_blad(int blad)
{
    switch (blad)
    {
	case 0:
		break;
	case -1:
	    	fprintf(stderr,"BLAD: Niepoprawna skladnia lub nieznana opcja.\n");
	    	fprintf(stderr,"\nObsluga:\n./program -i nazwa_pliku_wej [-o nazwa_pliku_wyj] opcje\n");
	    	fprintf(stderr,"Opcje:\n");
            fprintf(stderr,"-d              wyswietla za pomoca display\n");
	    	fprintf(stderr,"-n              negatyw\n");
            fprintf(stderr,"-k              konturowanie\n");
	    	fprintf(stderr,"-p prog		progowanie\n");
            fprintf(stderr,"-r prom         romzmycie poziome\n");
            fprintf(stderr,"prog (progowanie) - liczba calkowita\n");
            fprintf(stderr,"prom (roozmycie poziome) - liczba calkowita\n");
	    	exit(-1);
	case -2:
	    	fprintf(stderr, "BLAD: Brak nazwy pliku po -i\n");
	    	exit(-2);
	case -3:
	    	fprintf(stderr, "BLAD: Brak nazwy pliku wyjsciowego po -o\n");
	    	exit(-3);
	case -4:
	    	fprintf(stderr, "BLAD: Nie podano progu lub prog spoza zakresu [0;max_odcien].\n");
	    	exit(-4);
	case -5:
	    	fprintf(stderr, "BLAD: Nie podano promienia lub mniejszy od zera.\n");
	    	exit(-5);
	case -6:
	    	fprintf(stderr,"BLAD: Nalezy podac plik wejsciowy.\n");
	    	fprintf(stderr,"\nObsluga:\n./program -i nazwa_pliku_wej [-o nazwa_pliku_wyj] opcje\n");
	    	fprintf(stderr,"Opcje:\n");
	    	fprintf(stderr,"-d		wyswietla za pomoca display\n");
	    	fprintf(stderr,"-n		negatyw\n");
	    	fprintf(stderr,"-k		konturowanie\n");
            fprintf(stderr,"-p prog		progowanie\n");
            fprintf(stderr,"-r prom   	rozmycie poziome\n");
            fprintf(stderr,"prog (progowanie) - liczba calkowita\n");
            fprintf(stderr,"prom (rozmycie poziome) - liczba calkowita\n");
	    	exit(-1);
	    	exit(-6);
	default:
	    	fprintf(stderr, "BLAD: Nieznany blad.\n");
	    	exit(-7);
    }
}
