#ifndef MANIPULATOR_HH
#define MANIPULATOR_HH
#include "Wektor.hh"
#include "Macierz.hh"
#include <vector>
#include <iostream>



/*************************************************************************/
//------------------------ Manipulator ----------------------------------
/*************************************************************************/

/**
* @brief Klasa modeluje pojecie manipulatora jako zbior wierzcholkow przegubow, dlugosci ogniw i katow obrotu
*/

class Manipulator {


public:

/**
* @brief kontener typu vector na wspolrzedne przegubow jako wektory
*/

 std::vector<Wektor <double> > Pozycje;

/**
* @brief kontener typu vector na dlugosci ogniw manipulatora
*/

 std::vector<double> DlugosciOgniw;


/**
* @brief kontener typu vector na wartosci katow postury manipulatora
*/

  std::vector<double> KatNachylenia;

/**
* @brief konsruktor klasy Manipulator
*/

	Manipulator () {DlugosciOgniw.clear();
			KatNachylenia.clear();
			Wektor<double> Wek;
			Wek.wstaw(0, 0);
			Wek.wstaw(1, 0);
			Pozycje.push_back(Wek);}



/**
 * @brief Oblicza pozycje przegubow manipualtora
 * 	Obraca wektor wedlug shcematu obrotu i
 *  	wstawia wyniki do Pozycje
 * @return true jezeli ilosc ogniw jest rowna ilosci katow
 *		false w przeciwnej sytuacji
 *
 */

	bool ObliczPoz();



/**
 * @brief Ustawia manipulator do pionu wstawiajac odpowiednie katy
 *
 */

	void ZrobPion();




/**
 * @brief Zapsiuje wspolrzedne do pliku "poczatek.txt"
 *
 */

	void ZapiszPoczatek();

};



/**
 * @brief umozliwia wczytanie wektorow na strumieniu wyjsciowym.
 *
 *
 *   @param Strm strumien wyjsciowy, na ktorym ma byc wyswietlony
 *       nasz wektor.
 *   @param Pr przechowuje zadane wektory
 *
 *
 * @return Referencja do strumienia, na ktorym wykonana zostala
 *   operacja odczytu wektorow w postaci wspolrzednych kwadratu.
 */

std::istream& operator >> ( std::istream &Strm, Manipulator &Man);



/**
 * @brief Umozliwia pobranie wektorow i wyswietlenie wsp.
 *  kwadratu na strumieniu wyjsciowym.
 *
 *   @param StrmWy strumien wyjsciowy, na ktorym ma byc wyswietlony
 *       nasz Prostopadloscian
 *   @param Pr obiekt z ktorego zostaja pobrane odpowiednie wektory
 *
 *
 *
 *  @return Strumien, na ktorym zostana wyswietlone wektory symbolizujace wspolrzedne
 *   wierzcholkow prostopadloscianu
 */
std::ostream& operator << (std::ostream &Strm, Manipulator &Man);

inline double DegToRad(double deg)
{
  return deg*3.14159265/180;
}

#endif
