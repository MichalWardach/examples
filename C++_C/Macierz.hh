#ifndef MACIERZ_HH
#define MACIERZ_HH
#define PI 3.14159265
#include "Wektor.hh"
#include <cmath>
#include <iostream>



/*************************************************************************/
//------------------------ Macierz ----------------------------------
/*************************************************************************/

/**
* @brief Klasa modeluje pojecie macierz jako dwa wektory
*/
template <typename Typ>
class Macierz{


/**
* @brief Tablica dwoch wektorow
*/

	Wektor<Typ> Mac[2];
	
	public:

/**
* @brief konsruktor klasy Macierz
*/

	Macierz () {}


/**
  * @brief Wstawia wektory na odpowiednia pozycje do danej macierzy
  *   
  *  @param pozycja musi byc liczba typu int
  *  @param W przechwuje nowy wektor do wstawienia do macierzy
  *
  */

	//void wstawWektor(Wektor<Typ> Wek, int wiersz);




/**
 * @brief przeciaza mnozenie macierzy i wektora
 *  Warunki koncowe:
 *   Po wykananiu funckji nastepuje mnozenie wsakzanych elementow
 * 
 *  @param Wek liczby przeksztalcone wczesniej w wektor gotowy do dzialania
 *  
 *
 *  @return Wektor Wtmp
 *   
 */


Wektor<Typ> operator * ( Wektor <Typ>Wek);

Wektor<Typ> operator [] (int wiersz) const;

void wstaw(int pozycja, Wektor <Typ> W);



};



template <typename Typ>
void Macierz<Typ>::wstaw (int pozycja, Wektor<Typ> W)
{
	 Mac[pozycja] = W;
}



template <typename Typ>
Wektor<Typ> Macierz<Typ>:: operator [] (int wiersz) const
{

  Wektor<Typ> newWek;
  for(int i=0; i < 2 ; i++)
    {
      
      newWek.wstaw(i, Mac[wiersz][i] );
    } 
  return newWek;
}

template <typename Typ>
Wektor<Typ> Macierz<Typ>::operator * (Wektor<Typ> Wek)
  {
	Wektor<Typ> Iloczyn;
	Iloczyn.wstaw(0,  (Mac[0][0]*Wek[0] + Mac[0][1]*Wek[1]));
	Iloczyn.wstaw(1,  (Mac[1][0]*Wek[0] + Mac[1][1]*Wek[1]));
	return Iloczyn;
  }

/**
 * @breif Umozliwia wczytanie macierzy na strumieniu wyjsciowym.
 * 
 *   @param StrmWy strumien wyjsciowy, na ktorym ma byc wyswietlona
 *       nasza macierz.
 *   @param Mac przechowuje zadane wektory
 *  
 *
 *  @return Referencja do strumienia, na ktorym wykonana zostala
 *   operacja odczytu liczb w postaci macierzy.
 */

template <typename Typ>
std::istream& operator >> (std::istream &Strm, Macierz<Typ> &Mac)
{

  Typ W;
  for (int i=0; i < 2 ; i++)
    {
      Strm >> W;
      Mac.wstaw(i, W);
    }

  return Strm;
}







/**
 * @brief Umozliwia pobranie wektorow i wyswietlenie macierzy na strumieniu wyjsciowym.
 * 
 *   @param StrmWy strumien wyjsciowy, na ktorym ma byc wyswietlona
 *       nasza macierz
 *   @param Mac macierz z ktorej pobierane sa wektory
 *       
 *
 *  @return Strumien, na ktorym zostanie wyswietlona macierz
 */

template <typename Typ>
std::ostream& operator << (std::ostream &Strm, const Macierz<Typ> &Mac)
 
{
  for (int i=0; i < 2 ; i++)
    {
      Strm << Mac[i] << std::endl ;
    }
     
  return Strm;
}



#endif
