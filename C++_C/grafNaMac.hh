#ifndef GRAFNAMAC_HH
#define GRAFNAMAC_HH
#include "graf.hh"
#include <vector>
#include <fstream>
#include <iomanip>
/**
  * @brief klasa obrazujaca pojecie grafu na podstawie macierzy sasiedztwa
  */
class GrafNaMac: public Graf
{
    public:
/**
  * @brief Macierz sasiedztwa
  */
    int Macierz[100][100];
/**
  * @brief konstruktor klasy wypelniajacy macierz zerami
  */
    GrafNaMac ();
/**
  * @brief destruktor klasy
  */
    ~GrafNaMac (){};
/**
  * @brief wstawia wartosc 1 gdy jest polaczenie miedzy wierzcholkami
  */
    void uzupelnijMacierz();
/**
  * @brief wypisuje macierz sasiedztwa w czytelnej formie
  */
    void wypiszMacierz();
};


#endif


