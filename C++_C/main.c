#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include "funkcje.h"
#include "opcje.h"

#define DL_LINII 128

/***************************************************************/
/* Funkcja odczytujaca obrazek z pliku i zapisujaca go do      */
/* struktury.						       */
/*							       */
/* *p - wskaznik na plik				       */
/* *obraz - wskaznik na strukture przechowujaca dane o obrazie */
/*							       */
/* PRE: poprawny uchwyt do pliku			       */
/* POST: funkcja zapisuje obraz z pliku do tablicy w strukturze*/
/***************************************************************/

int odczyt(FILE *p, t_obraz *obraz) 
{
  char s[DL_LINII];
  int znak,koniec=0,i,j;

  /* sprawdzenie czy podano prawidlowy uchwyt pliku */
  if (p==NULL) 
  	{
    	fprintf(stderr,"BLAD: Nie podano nazwy pliku.\n");
    	return(0);
  	}
  if (fgets(s,DL_LINII,p)==NULL) koniec=1;


  /* Sprawdzenie "numeru magicznego - powinien byc P2 */
  if ( (s[0]!='P') || (s[1]!='2') || koniec) 
  	{
    	fprintf(stderr,"BLAD: To nie jest plik PGM.\n");
    	return(0);
  	}

  /* Pominiecie komentarzy */
  do 
  	{
    	if ((znak=fgetc(p))=='#') 
    		{
      		if (fgets(s,DL_LINII,p)==NULL) koniec=1;
    		}  
	else 
    		{
      		ungetc(znak,p);
    		}	
  	} while (! koniec && znak=='#');

  /* Pobranie wymiarow i liczby odcieni szarosci */
  if (fscanf(p,"%d %d %d",&obraz->wym_x,&obraz->wym_y,&obraz->odcien)!=3) 
  	{
    	fprintf(stderr,"BLAD: Niepoprawne wymiary obrazka lub skala szarosci.\n");
    	return(0);
  	}       

  obraz->piksele = malloc(obraz->wym_x*obraz->wym_y*sizeof(int));

  /* definiuje wskaznik na piksele */
  int (*piksele)[obraz->wym_y];
  piksele=(int(*)[obraz->wym_y]) obraz->piksele;

  /* Pobranie obrazu i zapisanie w tablicy */
  for (j=0;j<obraz->wym_y;j++) 
  	{
    		for (i=0;i<obraz->wym_x;i++) 
    		{
			if (fscanf(p,"%d",(&piksele[i][j]))!=1) 
			{
			fprintf(stderr,"BLAD: Niepoprawne wymiary obrazka.\n");
			return(0);
			}
    		}
  	}
  return (0);
}

/*****************************************************/
/* Funkcja zapisujaca obraz do pliku *p_wyj wskaznik */
/* na plik wyjsciowy				     */
/*						     */
/* *obraz - struktura z danymi obrazu		     */
/*						     */
/* PRE: poprawny uchwyt do pliku, poprawnie wczytany */
/* 	obraz					     */
/* POST: funkcja zapisuje obrazek ze struktury do    */
/*	 pliku					     */
/*****************************************************/

int zapisz(FILE *plik, t_obraz *obraz) 
{
  int i,j;

  /*definiuje wskaznik na piksele*/
  int (*piksele)[obraz->wym_y];
  piksele=(int(*)[obraz->wym_y]) obraz->piksele;

  /* Zapisanie numeru magicznego - P2 */
  fprintf(plik,"P2\n");

  /* Zapis wymiarow i liczby odcieni szarosci */
  fprintf(plik,"%d %d\n%d\n",obraz->wym_x,obraz->wym_y,obraz->odcien);
      
  /* Zapisanie tablicy do pliku */
  for (j=0;j<obraz->wym_y;j++) 
  	{
    	for (i=0;i<obraz->wym_x;i++) 
    		{
		fprintf(plik,"%d ",piksele[i][j]);
    		}
  	}
  fclose(plik);
  return (0);
}

/******************************************************/
/* Funkcja wyswietlajaca obraz			      */
/******************************************************/

void wyswietl(char *n_pliku) 
  {
  char polecenie[1024];
  /* wyswietl obrazek za pomoca imagemagick */
  strcpy(polecenie,"display ");
  strcat(polecenie,n_pliku);
  strcat(polecenie," &");  
  system(polecenie);
  }
  
/*****************************************************************/
/* 		TO JEST FUNKCJA MAIN!!!!!!!!!		 	 */
/*								 */
/* argc - liczba parametrow wywolania				 */
/* argv - wskaznik na tablice przechowujaca			 */
/*        parametry wywolania					 */
/*								 */
/*****************************************************************/

int main(int argc, char ** argv) 
{    
    t_obraz *obraz;
    t_opcje opcje;
    FILE *tmpfile;
    
    int q;

    obraz=(t_obraz *) malloc(sizeof(t_obraz));
    
    /* odczytujemy argumenty wywolania i sprawdzamy czy powstaly bledy */
    jaki_blad(opcje_czytaj(argc,argv,&opcje));
    
    /* wczytujemy parametry obrazu */
    odczyt(opcje.plik_in, obraz);

    int (*piksele)[obraz->wym_y];
    piksele=(int(*)[obraz->wym_y]) obraz->piksele;
  
    /* wykonywanie przeksztalcen zgodnie z kolejnoscia wprowadzania */    
    for (q=1; q<100;q++)
      	{
	if (opcje.progowanie==q) 
	jaki_blad(progowanie(obraz,opcje.prog));
	if (opcje.rozmycie_poziome==q) 
	jaki_blad(rozmycie_poziome(obraz,opcje.prom));	
	if (opcje.konturowanie==q) 
	konturowanie(obraz);
	if (opcje.negatyw==q) 
	negatyw(obraz);
      	}

    /* wyswietlenie obrazu przed jego zapisem */
    if (opcje.wyswietlenie==1) 
      	{
	/* stworzenie pliku tymczasowego */
	system("touch tmp.pgm");
	tmpfile = fopen("tmp.pgm","w");
	zapisz(tmpfile,obraz);
	wyswietl("tmp.pgm");
      	}

    /* ewentualne zapisanie obrazu */
    if (opcje.zapisz==1) 
       zapisz(opcje.plik_out, obraz);
    else 
    /* lub wyswietlenie na standardowym wyjsciu */
       zapisz(stdout,obraz);
       printf("\n");
 
    return 0;
}
