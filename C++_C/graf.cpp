#include "graf.hh"


void Graf :: wypiszGraf ()
{
    cout << "Ilosc wierzcholkow: " << ileWierzcholkow << endl;
    cout << "Ilosc krawedzi: " << ileKrawedzi << endl;
    for( size_t i = 0; i < listaKrawedzi.size(); i++ )
    {
        listaKrawedzi[i].wyswietl();
    }
}



void Graf :: odczytajGraf (char &argv)
{
  ifstream PlikWejscia; //zmienia strumieniowa do pracy z plikiem
  int a,b,c;
  Wierzcholek V,X;
  Krawedz K;


    PlikWejscia.open("graf.txt", std::ios::in);       // informacje o blednym dzialaniu z plikiem
    if(PlikWejscia == NULL)
      {
        cerr<<"\nNie otworzono pliku !";
      }

  if(!PlikWejscia.good())
     {
        cerr<<"\nBlad wczytywania tresci pliku.";
     }

        PlikWejscia >> ileWierzcholkow;
        PlikWejscia >> ileKrawedzi;
		for(int i=0; i<ileKrawedzi; i++)       // wprowadz wierzcholki i krawedzie
        {

        PlikWejscia >> a;
        PlikWejscia >> b;
        PlikWejscia >> c;
        V.utworz(a);                          //utworz wierzcholek poczatkowy
        X.utworz(b);                          //utworz wierzcholek koncowy
        listaWierzcholkow.push_back(V);
        listaWierzcholkow.push_back(X);
        K.zrobKrawedz(V,X,c);                // zrob krawedz z utworzonych wierzcholkow o wadze c
        listaKrawedzi.push_back(K);
        }

 	PlikWejscia.close();        //zamkniecie pliku

}

