#ifndef KRAWEDZ_HH
#define KRAWEDZ_HH
#include <iostream>
#include "wierzcholek.hh"


/**
  * @brief klasa obrazujaca pojecie krawedzi
  */
class Krawedz
{
    public:
/**
  * @brief poczatkowy wierzcholek krawedzi
  */
    Wierzcholek poczatek;
/**
  * @brief koncowy wierzcholek krawedzi
  */
    Wierzcholek koniec;
/**
  * @brief waga krawedzi
  */
    int waga;
/**
  * @brief konstruktor klasy Krawedz
  */
    Krawedz (){};
/**
  * @brief destruktor klasy Krawedz
  */
    ~Krawedz (){};
/**
  * @brief tworzy krawedz z dwoch wierzcholkow i wagi
  *
  * @param wierzcholek poczatkowy
  *
  * @param wierzcholek koncowy
  *
  * @param waga krawedzi
  */
    void zrobKrawedz (Wierzcholek a, Wierzcholek b, int w);
/**
  * @brief wyswietla krawedz w czytelnej postaci
  */
    void wyswietl ();

};


#endif
