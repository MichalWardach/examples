#include "grafNaList.hh"

void GrafNaList :: wypiszSasiadow ()
{
    for (int i=0; i<ileWierzcholkow; ++i)
    {
        cout << i << ": ";
        for (int j=0; j<Sasiedzi[i].size(); ++j)   //wypisz wszystkich s�siad�w wierzcho�ka i
        {
            cout << Sasiedzi[i][j] << " "; //wypisz j-tego s�siada wierzcho�ka o numerze i
        }
        cout << endl;

    }

}


void GrafNaList :: dodajSasiadow ()
{
    Krawedz tmp;
    int a,b;
    for( size_t i = 0; i < listaKrawedzi.size(); i++ )
    {
        tmp = listaKrawedzi[i];
        a = tmp.poczatek.pobierz();
        b = tmp.koniec.pobierz();
        Sasiedzi[a].push_back(b);      //dodanie jednego kierunku krawedzi
        Sasiedzi[b].push_back(a);      //dodanie kierunku powrotnego (graf nieskierowany)
    }
}

