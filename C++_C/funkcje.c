#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"funkcje.h"
#include"opcje.h"


/*******************************************************/
int negatyw(t_obraz *obraz) 
{
    int i,j;

    /* deklarujemy wskaźnik na piksele */
    int (*piksele)[obraz->wym_y];
    piksele=(int(*)[obraz->wym_y]) obraz->piksele;

    /* działamy na naszych pikselach */
    for (j=0;j<obraz->wym_y;j++) 
    {
	for (i=0;i<obraz->wym_x;i++) 
	{
	    piksele[i][j]=obraz->odcien-piksele[i][j]; 
    	}
    }
    return (0);
}
/********************************************************/


int progowanie(t_obraz *obraz, int prog) 
{
    int i,j;

    /* deklarujemy wskaznik na piksele */
    int (*piksele)[obraz->wym_y];
    piksele=(int(*)[obraz->wym_y]) obraz->piksele;

   if ((prog < 0) || (prog > obraz->odcien))
          return (-4); 

    /* zamieniamy wartosc piksela na 0 jesli byla 
       ona mniejsza lub rowna progowi i na maksymalny
       odcien, jezeli byla od niego wieksza */
    for (j=0;j<obraz->wym_y;j++)
    {
	for (i=0;i<obraz->wym_x;i++) 
	{
	    if (piksele[i][j] <= prog) piksele[i][j]=0;
	    else piksele[i][j]=obraz->odcien;
        } 
    }
    return (0);
}
/*******************************************************/


int konturowanie(t_obraz *obraz)
{
    int i,j;

    /* deklarujemy wskaznik na piksele */
    int (*piksele)[obraz->wym_y];
    piksele=(int(*)[obraz->wym_y]) obraz->piksele;

    /* wykonujemy konturowanie */
    for (j=0;j<obraz->wym_y-1;j++) 
    {
	for (i=0;i<obraz->wym_x-1;i++) 
	{
	    piksele[i][j]=(abs(piksele[i+1][j]-piksele[i][j])
	    +abs(piksele[i][j+1]-piksele[i][j])); 
        } 
    }
    /* maksimum i minimum potrzebne do normalizacji */
    float max, min;
    /* uzyskuje minimum i maksimum szarosci pikseli
       dla operacji normalizacji */
    
    for (j=1;j<obraz->wym_y-1;j++) 
    {
	for (i=1;i<obraz->wym_x-1;i++) 
	{
	    if (i==1 && j==1)
                max=min=piksele[1][1];
	    if (piksele[i][j]>max) 
                max=piksele[i][j];
	    if (piksele[i][j]<min)
                min=piksele[i][j]; 
	}
    }
   
    /* proces normalizacji - sprowadzamy wartosci pikseli do 
    odpowiedniego zakresu i zapisujemy je do tablicy */
    for (j=1;j<obraz->wym_y-1;j++) 
    {
	for (i=1;i<obraz->wym_x-1;i++)
	piksele[i][j]=(((piksele[i][j]-min)/(max-min))*obraz->odcien);
    }  

    return (0);
}


int rozmycie_poziome(t_obraz *obraz, int prom)
{
 int i,j;

    /* deklarujemy wskaznik na piksele */
    int (*piksele)[obraz->wym_y];
    piksele=(int(*)[obraz->wym_y]) obraz->piksele;
	
	if (prom<=0)
	  return (-5);

 for (j=0;j<obraz->wym_y;j++)
    {
	for (i=0;i<obraz->wym_x-prom;i++) 
	{
		if(i<prom){ piksele[i][j]=(piksele[i][j]+piksele[i+prom][j])/2; }
		else if(obraz->wym_x<prom+i){ piksele[i][j]=(piksele[i][j]+piksele[i-prom][j])/2; }
	    	else piksele[i][j]=(piksele[i-prom][j]+piksele[i][j]+piksele[i+prom][j])/3;
	}
    }
	return (0);
}





