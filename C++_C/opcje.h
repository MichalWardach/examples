/**************************************************************/
/* Tworzymy strukture opcje w ktorej zapisywane sa parametry  */
/* wywolania programu					      */
/*							      */
/* plik_in - uchwyt do pliku wejsciowego		      */
/* plik_out - uchwyt do pliku wyjsciowego		      */
/* negatyw, progowanie, konturowanie, wyswietl, zapisz -      */
/* zmienne przechowujace informacje o argumentach wywolania   */
/*						 	      */ 
/**************************************************************/

typedef struct 
	{
  	FILE *plik_in, *plik_out, *plik_polacz;       
  	int  progowanie, rozmycie_poziome, konturowanie, negatyw, wyswietlenie, zapisz;
  	int prog; 
	int prom;         
 	} t_opcje;

/************************************************************************/
/*   Funkcja odczytuje argumenty wywolania i zapisuje je w tablicy	*/
/*									*/
/*   argc - ilosc argumentow wywolania					*/
/*   argv - wartosci argumentow wywolania				*/
/*   *wybor - struktura zawierajaca dostepne opcja 			*/	
/*									*/   
/*   PRE: patrz komentarze	 					*/
/*   									*/
/*   POST:funkcja pobiera argumenty wywolania i wypelnia tablice 	*/
/*   	  opcjami   							*/
/*									*/
/*   utworzono z wykorzystaniem funkcji pobranej z:                     */
/*   http://sequoia.ict.pwr.wroc.pl/~mucha/PProg/opcje.c                */
/*   COPYRIGHT (c) 2007 ZPCiR					 	*/
/************************************************************************/

int opcje_czytaj(int argc, char **argv, t_opcje *wybor);

/********************************************************/
/* Funkcja obslugujaca bledy programu	                */
/*						        */
/* blad - kod bledu zwrocony przez funkcje opcje_czytaj */
/*						        */
/* PRE:                                                 */                    
/* kod bledu zwrocony przez funkcje opcje_czytaj        */
/* POST:					        */
/* funkcja konczy prace programu		        */
/********************************************************/

int jaki_blad(int blad);
