#ifndef WEKTOR_HH
#define WEKTOR_HH
#include <iostream>
/*************************************************************************/
//------------------------ Wektor ----------------------------------
/*************************************************************************/

/**
* @brief Klasa modeluje pojecie wektora jako tablice dwuelementowa
*/
template <typename Typ>
class Wektor {

/**
* @brief Tablica dla wektora o polach typu double
*/
	//Typ wek[Rozmiar];

	Typ wek[2];
 public:

/**
* @brief konsruktor klasy Wektor
*/

	Wektor () {}



/**
  * @brief Pobiera liczbe do wstawienia na odpowiednia pozycje do danego wektora 
  *  
  * @param pozycja musi byc liczba typu int
  *
  * @return Odpowiednie pole wektora
  */
	
	Typ pobierz(int pozycja) const
	{
   		return  wek[pozycja]  ; 
	}
	

/**
  * @brief Wstawia liczbe na odpowiednia pozycje do danego wektora 
  *  
  *  @param pozycja musi byc liczba typu int
  *  @param liczba musi byc typu int
  *
  */

	void wstaw (int pozycja, Typ liczba)
	{
	 wek[pozycja] = liczba;
	}	




/**
 * @brief przeciaza dodawanie wektorow
 * Warunki koncowe:
 *  Po wykananiu funckji nastepuje dodawanie wsakzanych elementow
 *  @param Wec do tego wektora zostaja wstawione wyniki dodawania
 *  	       odpowiednich elementow wektorow    	   
 *
 *  
 *
 *  @return Wtmp
 *   
 */

Wektor operator + ( Wektor V);

/**
 * @brief przeciaza odejmowanie wektorow
 * Warunki koncowe:
 *  Po wykananiu funckji nastepuje odejmowanie wsakazanych elementow
 *  @param Wec do tego wektora zostaja wstawione wyniki odejmowania
 *  	       odpowiednich elementow wektorow    	   
 *
 *  
 *
 *  @return Wtmp
 *   
 */
Wektor operator - ( Wektor V);


Typ operator [] (int kolumna) const;

Wektor operator = (const Wektor wzor);

};


template <typename Typ>
Wektor<Typ> Wektor<Typ> :: operator = (const Wektor<Typ> wzor)
{
  for(int i=0; i<2; i++)
    {
      wek[i] = (wzor.wek)[i];
    }
  return *this;
}


template <typename Typ>
Wektor<Typ> Wektor<Typ>:: operator + ( Wektor<Typ> V)
{
  Wektor<Typ> V2;
  Typ suma=0;
  for(int i=0; i<2 ; i++)
    {
      suma = pobierz(i) + V.pobierz(i);
      V2.wstaw(i, suma);    
    }
  return V2;
}

template <typename Typ>
Wektor<Typ> Wektor<Typ>:: operator - ( Wektor<Typ> V)
{
  Wektor<Typ> V2;
  Typ suma=0;
  for(int i=0; i<2 ; i++)
    {
      suma = pobierz(i) - V.pobierz(i);
      V2.wstaw(i, suma);    
    }
  return V2;
}


/**
 * @brief Umozliwia wczytanie wektora na strumieniu wyjsciowym.
 * 
 *   @param StrmWy strumien wyjsciowy, na ktorym ma byc wyswietlony
 *       nasz wektor.
 *   @param liczba przechowuje zadana liczbe
 *  
 *
 *   @return Referencja do strumienia, na ktorym wykonana zostala
 *   operacja odczytu liczb w postaci wektora.
 */
template <typename Typ>
std::istream& operator >> (std::istream &Strm, Wektor<Typ> &Wek)
{
  Typ liczba;
   for (int i=0; i < 2 ; i++)
    {
      Strm >> liczba;
      Wek.wstaw(i,liczba);
    }
	return Strm;


}


template <typename Typ>
Typ Wektor<Typ>:: operator [] (int kolumna) const
{
  return ( wek[kolumna] ) ; 
}


/**
 * @brief Umozliwia pobranie liczb i wyswietlenie wektora na strumieniu wyjsciowym.
 * 
 *   @param StrmWy strumien wyjsciowy, na ktorym ma byc wyswietlony
 *       nasz wektor
 *
 *  @return Strumien, na ktorym zostanie wyswietlony symbol w postaci wektora.
 */

template <typename Typ>
std::ostream& operator << (std::ostream &Strm, const Wektor<Typ> &Wek)
{
  for(int i=0; i< 2 ; i++)
    {
      Strm<<Wek[i];
    }
     
  return Strm;
}



#endif
