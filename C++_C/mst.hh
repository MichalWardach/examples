#ifndef MST_HH
#define MST_HH
#include <iostream>
#include <queue>
#include "wierzcholek.hh"
#include "krawedz.hh"
#include "graf.hh"

using namespace std;
/**
  * @brief funktor do porwnan wierzcholkow w kolejce priorytetowej odwiedzonych
  */
struct porownajWierzcholki
{
    bool operator ()( const Wierzcholek & w1, const Wierzcholek & w2 )  //kolejnosc rosnaca
    {
        if( w1.wartosc > w2.wartosc ) return true;

        if( w1.wartosc < w2.wartosc ) return false;

        return false;
    }
};
/**
  * @brief funktor do porwnan krawedzi w kolejkach priorytetowych krawedzi i krawedzi incydentnych
  */
struct porownajKrawedzie
{
    bool operator ()( const Krawedz & kr1, const Krawedz & kr2 )  //kolejnosc rosnaca
    {
        if( kr1.waga > kr2.waga ) return true;

        if( kr1.waga < kr2.waga ) return false;

        return false;
    }
};




/**
  * @brief Klasa przedstawiajaca minimalne drzewo rozpinajace
  */
class Mst
{
    public:

    vector< pair<int,int> > tabPar[100000];  //tablica par wiercholka i kosztu jego odleglosci od wybranego wierzcholka
/**
  * @brief konstruktor klasy Mst
  */
    Mst () {}
/**
  * @brief destruktor klasy Mst
  */
    ~Mst (){};
    void utworzPary (Graf G);

/**
  * @brief sprawdza czy podany wierzcholek zostal juz odwiedzony
  *
  * @param wierzcholek do sprawdzenia
  *
  * @return prawda jezeli odwiedzony, falsz w przeciwnym wypadku
  */
    bool czyOdwiedzony (Wierzcholek W);
/**
  * @brief dodaje wierzcholek do drzewa
  *
  * @param wierzcholek do dodania
  */
    void dodajWierzcholek (Wierzcholek W);
/**
  * @brief dodaje krawedz do kolejki priorytetowej
  *
  * @param dodawana krawedz
  */
    void dodajKrawedz (Krawedz K);
/**
  * @brief sprawdza czy wszystkie wierzcholki zostaly odwiedzone
  *
  * @return prawda jezeli wszystkie sa odwiedzone, falsz w przeciwnym wypadku
  */
    bool czyWszystkie ();

/**
  * @brief znajduje krawedzie incydentne do zadanego wierzcholka w podanym grafie
  *
  * @param wierzcholek dla ktorego maja byc znalezione krawedzie incydentne
  *
  * @param graf w ktorym maja byc znalezione krawedzie
  */
    void znajdzIncydentne (Wierzcholek W, Graf G);
/**
  * @brief zdejmuje i usuwa krawedz z kolejki krawedzi o najmniejszej wadze
  *
  * @param kopia znalezionej krawedzi
  */
    Krawedz zdejmijKrawedz ();
/**
  * @brief zdejmuje i usuwa krawedz incydenta z kolejki krawedzi incydentnych o najmniejszej wadze
  *
  * @param kopia znalezionej krawedzi
  */
    Krawedz zdejmijIncydentna ();
/**
  * @brief kolejka priorytetowa wierzcholkow grafu
  */
    priority_queue < Wierzcholek, vector < Wierzcholek >, porownajWierzcholki > odwiedzone;
/**
  * @brief kolejka priorytetowa krawedzi grafu
  */
    priority_queue < Krawedz, vector < Krawedz >, porownajKrawedzie > kolejkaKrawedzi;
/**
  * @brief kolejka priorytetowa krawedzi incydentnych
  */
    priority_queue < Krawedz, vector < Krawedz >, porownajKrawedzie > incydentne;

};









#endif

