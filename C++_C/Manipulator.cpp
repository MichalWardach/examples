#include "Manipulator.hh"
#define PI 3.14159265
#include <iomanip>
#include <fstream>

std::ostream& operator << (std::ostream &Strm, Manipulator &Man)
{
  Strm << std::fixed << std::showpoint;
  Strm<< std::setprecision(2);
  Strm << "\nKonfiguracja:";
  for(unsigned int i=0; i <Man.DlugosciOgniw.size()-1 ; i++)
    Strm<<"\nDlugosc ogniwa "<<i<<": "<<Man.DlugosciOgniw[i];
    
  Strm << "\n\nPostura:";
  for(unsigned int i=0; i <Man.KatNachylenia.size() ; i++)
	if (i!=4){
    Strm<<"\nq"<<i<<" :   "<<Man.KatNachylenia[i] << "\tPrzegub "<< i << ":  " << "(" 
    <<(Man.Pozycje[i])[0]<<", "<<(Man.Pozycje[i])[1]<<")";}
	else {
    Strm<<"\n\t\t  Efektor:  "<<"("<<(Man.Pozycje[i])[0]<<", "<<(Man.Pozycje[i])[1]<<")";}

  return Strm;
}




bool Manipulator:: ObliczPoz()
{
  Macierz<double> MacObrotu;
  Wektor<double> WektorDlugosciOgniwa;
  Wektor <double> WektorMac1;
  Wektor <double> WektorMac2;
  double deltaNachylenia=0;



  if(DlugosciOgniw.size() != KatNachylenia.size() )
    {
      return true;
    }

  for(unsigned int i=1; i< DlugosciOgniw.size()+1 ; i++ )
    {
      
      if(i==1) deltaNachylenia = (KatNachylenia)[0];
      else deltaNachylenia=  deltaNachylenia  + KatNachylenia[i-1];
      


	WektorMac1.wstaw(0, cos(DegToRad(deltaNachylenia)));
	WektorMac1.wstaw(1, -sin(DegToRad(deltaNachylenia)));
	WektorMac2.wstaw(0, sin(DegToRad(deltaNachylenia)));
	WektorMac2.wstaw(1, cos(DegToRad(deltaNachylenia)));
	MacObrotu.wstaw(0, WektorMac1);
	MacObrotu.wstaw(1, WektorMac2);

      
      WektorDlugosciOgniwa.wstaw(0, DlugosciOgniw[i-1]);
      WektorDlugosciOgniwa.wstaw(1, 0);
   

      WektorDlugosciOgniwa= MacObrotu * WektorDlugosciOgniwa;
      
    
      WektorDlugosciOgniwa= WektorDlugosciOgniwa + Pozycje[i-1];
      

      Pozycje.push_back( WektorDlugosciOgniwa);
      
    }
  
  return true;
}



void Manipulator:: ZrobPion()
{
  KatNachylenia.push_back( 90);
  for(unsigned int i=1 ; i< DlugosciOgniw.size() ; i++ )    
    KatNachylenia.push_back(0);
 
}

void Manipulator::ZapiszPoczatek()
{
double x,y;
std::ofstream plik;

plik.open( "poczatek.txt", std::ios::out );
if( plik.good() == true )
{
    for (unsigned int i=0; i<Pozycje.size(); i++)
	{
		x = Pozycje[i][0];
		plik << x << "\t\t";
		y = Pozycje[i][1];
		plik << y << "\n";
	}
} else std::cout << "Dostep do pliku zostal zabroniony!" << std::endl;



plik.close();

}








