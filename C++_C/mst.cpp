#include "mst.hh"


bool Mst :: czyOdwiedzony (Wierzcholek W)
{
    Wierzcholek Wtmp;
    for( size_t i = 0; i < odwiedzone.size(); i++ )
    {
        Wtmp = odwiedzone.top();
        odwiedzone.pop();                  //sciagaj kolejne krawedzie z koljeki priorytetowej
        if (Wtmp.pobierz() == W.wartosc)    //sprawdz czy wierzcholek jest na odpowiednim miejscu w kolejce
        {
            return true;
        }
    }
    return false;
}


void Mst :: dodajWierzcholek (Wierzcholek W)
{
    odwiedzone.push(W);
}


void Mst :: dodajKrawedz (Krawedz K)
{
    kolejkaKrawedzi.push(K);
}

Krawedz Mst :: zdejmijKrawedz ()
{
    Krawedz tmp;
    tmp = kolejkaKrawedzi.top();
    kolejkaKrawedzi.pop();
    return tmp;
}


Krawedz Mst :: zdejmijIncydentna ()
{
    Krawedz tmp;
    tmp = incydentne.top();
    incydentne.pop();
    return tmp;
}





void Mst :: znajdzIncydentne (Wierzcholek W, Graf G)
{
    for( size_t i = 0; i < G.listaKrawedzi.size(); i++ )
    {
        if (G.listaKrawedzi[i].poczatek.pobierz() == W.wartosc)  //znajdz krawedz o poczatku w podanym wierzcholku
        {
            incydentne.push(G.listaKrawedzi[i]);           //dodawanie krawdzi incydentnych do podanego wierzcholka w kolejce priorytetowej
        }
    }
}




bool Mst :: czyWszystkie ()
{
    Wierzcholek Wtmp;

    for( size_t i = 0; i < odwiedzone.size(); i++ )
    {
        Wtmp = odwiedzone.top();
        odwiedzone.pop();
        if (Wtmp.pobierz() == i)    //jezeli kolejny wierzcholek jest na odpwowiedniej pozycji w kolejce
        {
            continue;                 // kontynuuj sprawdzanie
        }
        else
        {
            return false;
        }
    }
    return true;
}


void Mst :: utworzPary (Graf G)
{
    Krawedz tmp;
    for( size_t i = 0; i < G.listaKrawedzi.size(); i++ )
    {
        tmp = G.listaKrawedzi[i];
        tabPar[tmp.poczatek.pobierz()].push_back( pair<int,int>(tmp.koniec.pobierz(),tmp.waga) );
    }



    //tabPar[a].push_back( pair<int,int>(b,c) );
}










