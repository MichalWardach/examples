#include<stdio.h>
#include<string.h>
#include<stdlib.h>



/**************************************************/
/* Tworzymy strukture t_obraz przechowujaca dane  */
/* o obrazie					  */
/**************************************************/

typedef struct
	{	
    	int wym_x, wym_y;
    	int odcien;
    	void *piksele;
	} t_obraz;

/******************************************************/
/*  Jest to funkcja negatyw. Funkcja ta tworzy obraz  */
/*  wyjsciowy bedacy negatywem obrazu wejsciowego     */
/*  jasnosc punktu w obrazie zrodlowym zmienia swoja  */
/*  wartosc na roznice pomiedzy maksymalnym odcieniem */
/*  szarosci a pierwotna wartoscia                    */
/*						      */
/*  PRE: poprawny wskaznik na nasz obraz              */
/*  POST: zwrocenie negatywu ubrazu i wartosci 0      */
/******************************************************/

int negatyw(t_obraz *obraz);

/*****************************************************/
/*  Jest to funkcja progowania. zadaniem tej funkcji */
/*  jest zmiana wartosci jasnosci poszczegolnych     */
/*  pikseli obrazu na 0 lub maksymalna; dla pikseli  */
/*  o jasnosci mniejszej badz rownej polowie         */
/*  maksymalnej wartosci skali, funkcja zmieni ta    */
/*  wartosc na 0, w przyadku jasnosci wiekszej,      */
/*  zwiekszy ja maksymalnie.                         */  
/*                                                   */
/*  PRE:poprawny wskaznik na obraz, poprawny prog    */
/*  POST:zwraca sprogowany obraz oraz wartosc 0      */
/*****************************************************/

int progowanie(t_obraz *obraz, int prog);

/********************************************************/
/*  Konturowanie polega na rozjasnieniu pikseli, ktore  */
/*  roznia sie od otoczenia i przyciemnieniu tych       */
/*  podobnych do otoczenia; mowiac o otoczeniu ma sie   */
/*  na mysli piksele lezace ponizej i na prawo od       */
/*  danego punktu				        */
/*						        */
/*  PRE: poprawny wskaznik na obraz		        */
/*  POST: zamiana wartosci pikseli w tablicy wg wzoru   */
/*        podanego w funkcji			        */
/********************************************************/

int konturowanie(t_obraz *obraz);

/******************************************************/
/*  Jest to funkcja rozmywania poziomego.             */
/*  Funkcja ta zmniejsza ostrc obrazu w kierunku      */
/*  poziomym. W celu obliczenia wartosci kazdego z    */
/*  pikseli jest usredniana wartosc jego jasnosci     */
/*  oraz jasnosc liczby pikseli zaleznej od promienia */
/* z lewej i prawej strony (w tym samym wierszu).     */
/*                                                    */
/*  PRE:poprawny wskaznik na obraz, poprawny prom     */
/*  POST:zwraca rozmyty obraz oraz wartosc 0          */
/******************************************************/

int rozmycie_poziome(t_obraz *obraz, int prom);

/*********************************************************/
/* Funkcja odczytujaca obrazek z pliku		 	 */
/* i zapisujaca go do struktury.			 */
/*							 */
/* PRE: poprawny uchwyt do pliku			 */	
/* POST: funkcja zapisuje obraz z pliku do tablicy	 */
/*       w strukturze					 */	
/*********************************************************/


int odczyt(FILE *p, t_obraz *obraz);
