#include "grafNaMac.hh"



GrafNaMac :: GrafNaMac ()
{
  for(int i = 0; i <100; i++)
  {
    for(int j = 0; j < 100; j++)
    {
      Macierz[i][j] = 0;
    }
  }
}

void GrafNaMac :: uzupelnijMacierz ()
{
    Krawedz tmp;
    int a,b;
    for( size_t i = 0; i < listaKrawedzi.size(); i++ )
    {
        tmp = listaKrawedzi[i];
        a = tmp.poczatek.pobierz();
        b = tmp.koniec.pobierz();
        Macierz[a][b] = 1;            //dla istniejacej krawedzi wstaw 1 w odpowiednie miejsce
	    Macierz[b][a] = 1;            //dodanie przebiegu tej samej krawedzi w druga strone (graf nieskierowany)
    }
}




void GrafNaMac :: wypiszMacierz ()
{
    for(int i = 0; i < ileWierzcholkow; i++)
    {
        for(int j = 0; j < ileWierzcholkow; j++)
        {
            cout << setw(3) << Macierz[i][j];
        }
        cout << endl;
    }

}

